操作步骤：

工具；使用android-ndk-r10，及android-sdk Build Tools 24, 23.0.X，

根据如下文档，准备GIT仓库

1. 找出包含JNI_onLoad ()文件是否在Android.mk中，以下以main.cpp为例

jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    JniHelper::setJavaVM(vm);

    return JNI_VERSION_1_4;
}

2. 所有除main.cpp外文件，编译成静态文件libgame_static；libgame.so引用libgame_static，例：

打开Android.mk中STEP101部分
关闭Android.mk中STEP103部分

3. 复制libgame_static.a

$ cp obj/local/armeabi/libgame_static.a jni/

4. 使用现编译的libgame_static.a，并发布到我方

关闭Android.mk中STEP101部分
打开Android.mk中STEP103部分



5. 修改buildproj.sh，并保证编译通过

$ ./buildproj.sh

6. 准备GIT仓库(包含jni/libgame_static.a)并提交到BitBucket（或其他），并告知我方相应地址(如https://bitbucket.org/newmobo/demo2d)

$ cd MyGame2D
$ git remote add origin https://mycompany@bitbucket.org/newmobo/mygame2d.git
$ git push -u origin --all
$ git push origin --tags




