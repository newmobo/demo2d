/*
 * Copyright(c) YummySDK 2015
 */

package org.yummysdk.sample;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.yummysdk.lib.YMBillingCallback;
import org.yummysdk.lib.YMBillingInterface;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class SampleService extends Service {

        @Override
        public void onCreate ()
        {
                super.onCreate ();

                YMBillingInterface.sinit (this, "PPDUMMY001", 0x400, null);
        }

        @Override
        public IBinder onBind (Intent intent)
        {
                return null;
        }
}
