/*
 * Copyright(c) YummySDK 2015
 */

package org.yummysdk.sample;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.yummysdk.lib.YMBillingCallback;
import org.yummysdk.lib.YMBillingInterface;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


public class SampleGame extends Cocos2dxActivity {

        YMBillingCallback       mBillingCallback;


        public void showToast (String toast)
        {
                final   String      fToast      = toast;
                SampleGame.this.runOnUiThread (new Runnable() {
                        @Override
                        public void run ()
                        {
                                Toast.makeText (SampleGame.this, fToast, Toast.LENGTH_SHORT).show ();
                        }
                });
        }

        protected void onCreate (Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);

                // There's no guarentee that the callback will be on any particular thread
                mBillingCallback        =
                new YMBillingCallback () {
                        @Override
                        public void onInitSuccess (String extra)
                        {
                                SampleGame.this.showToast ("初始化成功 extra: " + extra);
                        }

                        @Override
                        public void onInitFail (String extra, int code)
                        {
                                SampleGame.this.showToast ( "初始化失败: " + String.valueOf (code) +
                                                                " extra: " + extra );
                        }

                        @Override
                        public void onSuccess (String chargepoint)
                        {
                                SampleGame.this.showToast ("计费成功(" + chargepoint + ")");
                        }

                        @Override
                        public void onCancel (String chargepoint)
                        {
                                SampleGame.this.showToast ("计费取消(" + chargepoint + ")");
                        }

                        @Override
                        public void onFail (String chargepoint, int code)
                        {
                                SampleGame.this.showToast ( "计费失败(" + chargepoint + "): " +
                                                                String.valueOf (code) );
                        }
                };

                YMBillingInterface.init (this, "PPDUMMY001", 0x00, mBillingCallback);
        }



        public void onResume () {
                super.onResume ();
                // UMGameAgent.onResume(this);
        }

		public void onPause () {
                super.onPause ();
                // UMGameAgent.onPause(this);
		}


        static {
                System.loadLibrary ("game");
        }
}
