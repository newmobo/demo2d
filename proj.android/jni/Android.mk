LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)
LOCAL_MODULE     := yummy-prebuilt
LOCAL_SRC_FILES  := libyummy.so
include $(PREBUILT_SHARED_LIBRARY)


# STEP101: 制作game_static
include $(CLEAR_VARS)

LOCAL_MODULE := game_static
LOCAL_SRC_FILES := \
    ../../Classes/AppDelegate.cpp \
    ../../Classes/HelloWorld.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static

# LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
# LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

LOCAL_LDLIBS    += -llog -landroid
include $(BUILD_STATIC_LIBRARY)


# STEP103: 使用现编译的libgame_static
# include $(CLEAR_VARS)
# LOCAL_MODULE     := game_static
# LOCAL_SRC_FILES  := libgame_static.a
# include $(PREBUILT_STATIC_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := game
LOCAL_SRC_FILES := \
    hellocpp/main.cpp


LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES += game_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static

# LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
# LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

LOCAL_LDLIBS    += -llog -landroid
include $(BUILD_SHARED_LIBRARY)

# Newly added
$(call import-add-path,../cocos2d-x-2.2.6/cocos2dx/platform/third_party/android/prebuilt)

$(call import-module,cocos2dx)
# $(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
# $(call import-module,extensions)
# $(call import-module,external/Box2D)
# $(call import-module,external/chipmunk)

