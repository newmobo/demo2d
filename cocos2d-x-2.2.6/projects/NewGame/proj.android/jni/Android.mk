LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/HelloWorldScene.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

# WC additions
# LOCAL_CFLAGS    += -fvisibility=hidden
# LOCAL_LDLIBS    += -Wl,--gc-sections,--print-gc-sections
LOCAL_LDLIBS    += -Wl,--gc-sections
# LOCAL_LDLIBS    +=  -latomic


LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
# LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
# LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
# LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

include $(BUILD_SHARED_LIBRARY)

# Newly added
# http://stackoverflow.com/questions/9880488/compiling-cocos2d-cannot-find-module-with-tag-libjpeg
$(call import-add-path,/users/wc/Project/cocos2d-x-2.2.6/cocos2dx/platform/third_party/android/prebuilt)

$(call import-module,cocos2dx)
# $(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
# $(call import-module,extensions)
# $(call import-module,external/Box2D)
# $(call import-module,external/chipmunk)

