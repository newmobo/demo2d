LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/HelloWorldScene.cpp \
                   ../../Classes/GameOverScene.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

# LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static

# LOCAL_CFLAGS    += -fvisibility=hidden
# LOCAL_LDLIBS    += -Wl,--gc-sections,--print-gc-sections
LOCAL_LDLIBS    += -Wl,--gc-sections
# LOCAL_LDLIBS    +=  -latomic

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static


include $(BUILD_SHARED_LIBRARY)

$(call import-add-path,/users/wc//Project/YummySDK/scratch/color-switch/cocos2d-x-2.2.6/cocos2dx/platform/third_party/android/prebuilt)

$(call import-module,CocosDenshion/android)
$(call import-module,cocos2dx)
