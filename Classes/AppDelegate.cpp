#include "AppDelegate.h"
// #include "ManagerScene.h"
//#include "MobClickCpp.h"
#include "HelloWorld.h"

USING_NS_CC;

AppDelegate::AppDelegate() 
{

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() 
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(720, 1280, kResolutionShowAll);

    // enable High Resource Mode(2x, such as iphone4) and maintains low resource on other devices.
    // pDirector->enableRetinaDisplay(true);

    // turn on display FPS
    pDirector->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    //MOBCLICKCPP_START_WITH_APPKEY_AND_CHANNEL("0", "jidi");

    // create a scene. it's an autorelease object
    // CCScene *pScene = ManagerScene::scene();
    CCScene *pScene = HelloWorld::scene();

    // run
    pDirector->runWithScene(pScene);
    // glClearColor(255.0f, 255.0f, 255.0f, 1.0f);
    // glClearColor(255.0f, 0.0f, 0.0f, 1.0f);
    return true;
}


// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() 
{
	//umeng::MobClickCpp::applicationDidEnterBackground();
    CCDirector *d = CCDirector::sharedDirector();
    //d->stopAnimation();
    d->pause();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() 
{
	//umeng::MobClickCpp::applicationWillEnterForeground();
    CCDirector *d = CCDirector::sharedDirector();
    //d->startAnimation();
    d->resume();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}


