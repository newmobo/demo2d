#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
 
#include "cocos2d.h"
 
#include "SimpleAudioEngine.h"
 
//HelloWorld继承自CCLayer图层
class HelloWorld : public cocos2d::CCLayerColor
{
public:
  virtual bool init();
 
  static cocos2d::CCScene* scene();
 
  void menuCloseCallback(CCObject* pSender);
  CREATE_FUNC(HelloWorld);
};
 
#endif // __HELLOWORLD_SCENE_H__